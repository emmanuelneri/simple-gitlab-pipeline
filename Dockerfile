FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/simple-gitlab-pipeline-*.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]